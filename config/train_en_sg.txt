[data]
data_root             = /lfs01/workdirs/shams005/shams005u1/Datasets/B17_Train
data_names            = config/train_names_all_lggx3.txt 
modality_postfix      = [flair, t1, t1ce, t2]
label_postfix         = seg
file_postfix          = nii.gz
with_ground_truth     = True
batch_size            = 8
data_shape            = [19, 64, 64, 4]
label_shape           = [11, 64, 64, 1]
label_convert_source  = [0, 1, 2, 4]
label_convert_target  = [0, 0, 0, 1]
batch_slice_direction = sagittal
train_with_roi_patch  = True
label_roi_mask        = [1,4]
roi_patch_margin      = 5

[network]
net_type            = TF_MSNet
net_name            = TF_MSNet_EN32sg
downsample_twice    = False
class_num           = 2

[training]
learning_rate      = 1e-3
decay              = 1e-7
maximal_iteration  = 20000
snapshot_iteration = 4000
start_iteration    = 0
test_iteration     = 100
test_step          = 10
model_pre_trained  = 
model_save_prefix  = tf_msnet_en32sg

[ext]
type = 2
renorm = True
data_format = channels_last
